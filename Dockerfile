from ubuntu:xenial
run apt-get -y update
run apt-get install -y sudo curl git wget make libncurses-dev flex bison gperf python python-serial 
run curl https://raw.githubusercontent.com/lpodkalicki/blog/master/esp32/tools/build_esp32_toolchain.sh | bash -s -- -d ~/esp32
run cd $HOME/esp32 && wget https://dl.espressif.com/dl/xtensa-esp32-elf-linux64-1.22.0-61-gab8375a-5.2.0.tar.gz && tar -xzf xtensa-esp32-elf-linux64-1.22.0-61-gab8375a-5.2.0.tar.gz && export PATH=$PATH:$HOME/esp32/xtensa-esp32-elf/bin && echo "export PATH=\$PATH:$HOME/esp32/xtensa-esp32-elf/bin" >> ~/.bashrc
#run cd $HOME/esp32  && git clone --recursive https://github.com/espressif/esp-idf.git && export IDF_PATH=$HOME/esp32/esp-idf && echo "export IDF_PATH=$HOME/esp32/esp-idf" >> ~/.bashrc
#run cd $HOME/esp32/esp-idf/examples/get-started/blink  && make
run git clone https://github.com/lpodkalicki/blog.git && cd blog/esp32/016_wifi_sniffer
add sdkconfig /blog/esp32/016_wifi_sniffer/sdkconfig
